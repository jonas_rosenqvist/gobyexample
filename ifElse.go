package main

import "fmt"

func ifElse() {
	if true {
		fmt.Println("1")
	} else if false {

	} else {
		fmt.Println("2")
	}
}
