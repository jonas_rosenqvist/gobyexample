package main

import (
	"fmt"
)

const s string = "constant"

func constant() {
	fmt.Println(s)
	const n = 5000000
	const d = 3e20 / n

}
