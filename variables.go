package main

import (
	"fmt"
	"math"
)

func variables() {

	var b, c int = 1, 2
	fmt.Println(b, c)
	var d bool = true
	fmt.Println(d)

	var e int
	fmt.Print(e)

	f := "apple"
	fmt.Println(f)
	fmt.Println(math.Sin(float64(c)))
}
