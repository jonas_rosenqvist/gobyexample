package main

import "fmt"

func forFunc() {
	fmt.Println("forFunc()")
	i := 1
	for i < 3 {
		fmt.Println(i)
		i += 1
	}
	for i := 0; i < 10; i++ {
		fmt.Println(i)
	}
}
